<?php
class CourseRegister extends CActiveRecord {
    public $coursename;
    public $daynow;
    public $r_categorycourse;

    public function tableName() {
        return 'course_register';
    }

    public function rules() {
        return array(
            array('course_id, employee_id', 'required'),
            //  array('course_id, employee_id', 'numerical', 'integerOnly' => true),
            array('course_id','checkexites_course','on'=>'update'),
            array('note','required','on'=>'update'),
            array('idcourse', 'length', 'max' => 45),
            array('approval', 'length', 'max' => 2),
            array('time, note', 'safe'),
            array('id, idcourse, time, r_categorycourse,coursename,approval, note, course_id, employee_id', 'safe','on' => 'listcoursregister,search,listcourseapproval'),
        );
    }

    public function relations() {
        return array(
            'course' => array(self::BELONGS_TO, 'Course', 'course_id'),
            'checkcourse' => array(self::BELONGS_TO, 'CheckCourse', 'course_id'),
            'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'idcourse' => 'รหัสการสมัคร',
            'time' => 'เวลาสมัคร',
            'approval' => 'สถานะ',
            'note' => 'หมายเหตุ',
            'course_id' => 'รหัสหลักสูตร',
            'employee_id' => 'รหัสพนักงาน',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;     var_dump($this);exit();

        $criteria->compare('id', $this->id);
        $criteria->compare('idcourse', $this->idcourse, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('approval', $this->approval, true);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('course_id', $this->course_id);
        $criteria->compare('employee_id', $this->employee_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function listcourseapproval() {
        $criteria = new CDbCriteria;
        $criteria->together = true;
        $criteria->with = array('course');
        $criteria->compare('id', $this->id, true);
        $criteria->compare('approval', 1);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('idcourse', $this->idcourse, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('course_id', $this->course_id, true);
        $criteria->compare('Address', $this->employee_id, true);
        $criteria->addSearchCondition('course.coursename', $this->coursename);
        $criteria->group = 'course_id';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    } 

    public function listcourse_Noapproval($departments_id) {
        $criteria = new CDbCriteria;
        $criteria->together = true;
        $criteria->with = array('course');
        $criteria->compare('id', $this->id, true);
        $criteria->compare('approval', 2);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('idcourse', $this->idcourse, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('course_id', $this->course_id, true); 
        $criteria->compare('course.iddept', $departments_id);
        $criteria->addSearchCondition('course.coursename', $this->coursename);
        $criteria->group = 'course_id';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    } 
 
    public function listcoursregister($departments_id) {
        $criteria = new CDbCriteria; 
        $criteria->together = true;
        $criteria->with = array('course');
        $criteria->compare('id', $this->id, true);
        $criteria->compare('approval', $this->approval);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('course.iddept', $departments_id);
        $criteria->compare('course_id', $this->course_id, true);
        $criteria->compare('course.categorycourse', $this->r_categorycourse);
        $criteria->compare('course.name', $this->coursename, TRUE);
        
        //$criteria->addSearchCondition('course.name',$this->coursename,TRUE);
        $criteria->addInCondition('approval', array('0', '1', '2'), 'AND');
        $criteria->order='course.dayclose ASC';
        $criteria->group = 'course_id';
        /*
          return new CActiveDataProvider($this, array(
          'criteria'=>$criteria,
          )); */
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    'coursename' => array(
                        'asc' => 'course.name',
                        'desc' => 'course.name DESC',
                    ),
                    '*',
                ),
            ),
        ));
    }

    public function searchsavehistory($idcourse) {// ใช้ใน Employee action savehistory
        // $criteria = new CDbCriteria;
        // $criteria->with = array(
        //   'employee' => array('alias' => 't1', 'together' => true,),
        //  'employee.depart' => array('alias' => 't2', 'together' => true,),
        // );
        $criteria = new CDbCriteria;
        $criteria->together = true;
        //$criteria->with= array('employee','xCity','User');
        $criteria->with = array('employee');
        $criteria->addSearchCondition('course_id', $idcourse);
        $criteria->addInCondition('approval', array('1'), 'AND');
        // $criteria->addNotInCondition('approval', array('4','3','2','0'), 'AND');
        $criteria->group = 'employee_id';
        /*
          $criteria->compare('Id',$this->Id,true);
          $criteria->compare('Restaurant.Name',$this->Name,true);
          $criteria->addSearchCondition('xCountry.Name',$this->Country);
          $criteria->addSearchCondition('xCity.Name',$this->City);
          $criteria->compare('Zip',$this->Zip,true);
          $criteria->compare('Address',$this->Address,true);
          $criteria->compare('Description',$this->Description,true);
          $criteria->compare('Restaurant.Active',$this->Active,true);
          $criteria->addSearchCondition('User.Username',$this->Owner);
          $criteria->compare('Lat',$this->Lat);
          $criteria->compare('Lon',$this->Lon);
         */
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));
    }

    public static function checkRegister($id) {// จำนวนคนลงทะเบียน กำลังดำเนินการ+อณุมัติ
        $criteria = new CDbCriteria ();
        $criteria->condition = 'course_id=:cu_id';
        $criteria->params = array(
            ':cu_id' => $id
        );
        $criteria->addInCondition('approval', array('0', '1'), 'AND');

        $model = CourseRegister::model()->findAll($criteria);
        return count($model);
    }
    public static function checkRegisterAll($id) {// จำนวนคนลงทะเบียน กำลังดำเนินการ+อณุมัติ+คนไม่อณุมัติด้วย
        $criteria = new CDbCriteria ();
        $criteria->condition = 'course_id=:cu_id';
        $criteria->params = array(
            ':cu_id' => $id
        );
        $criteria->addInCondition('approval', array('0', '1','2'), 'AND');

        $model = CourseRegister::model()->findAll($criteria);
        return count($model);
    }
    
    public static function get_numalluser_complete($idcourse) {// จำนวนอบรมผ่านแล้ว
        $sql = 'SELECT count(*)as nums FROM course_register WHERE course_id="' . $idcourse . ' AND approval=4";';
        $dbCommand = Yii::app()->db->createCommand($sql);
        $data = $dbCommand->queryRow();
        return $data['nums'];
    }

    public static function checkmaxapprovel($id) {// จำนวนลงทะเบียนอณุมัติแล้ว
        $criteria = new CDbCriteria ();
        $criteria->condition = 'course_id=:cu_id';
        $criteria->addCondition('approval=:approval', 'AND');
        $criteria->params = array(
            ':cu_id' => $id, ':approval' => 1
        );
        $model = CourseRegister::model()->findAll($criteria);
        return count($model);
    }

    public function checuserregister($courseid, $user) {// return true ถ้าลงทะเีบยนไปแล้ว
        $criteria = new CDbCriteria ();
        $criteria->condition = 'course_id=:course AND employee_id=:user';
        $criteria->params = array(
            ':course' => $courseid, 'user' => $user
        );
        $model = CourseRegister::model()->findAll($criteria);
        return count($model) > 0;
    }

    public static function check_request_question_user($courseid, $user) {//เช็คว่าต้องทำแบบประเมินหรือไม่
        $criteria = new CDbCriteria ();
        $criteria->condition = 'approval=3 AND course_id=:course AND employee_id=:user';
        $criteria->params = array(
            ':course' => $courseid, 'user' => $user
        );
        // $criteria->addInCondition('approval', array('3'), 'AND');
        $model = self::model()->findAll($criteria);
        return count($model) > 0;
    }
    public static function is_showHistory_user($courseid, $user) {//เช็คว่าต้องทำแบบประเมินหรือไม่
        $criteria = new CDbCriteria ();
        $criteria->condition = 'course_id=:course AND employee_id=:user';
        $criteria->params = array(
            ':course' => $courseid, 'user' => $user
        );
        $criteria->addInCondition('approval',array('0','1','2','3'));
        // $criteria->addInCondition('approval', array('3'), 'AND');
        $model = self::model()->findAll($criteria);
        return count($model) > 0;
    }
    /*
      public static function  checkmaxcalcel($id) {
      $criteria = new CDbCriteria ();
      $criteria->condition = 'course_id=:cu_id';
      $criteria->addCondition('approval=:approval', 'AND');
      $criteria->params = array(
      ':cu_id' =>$id,':approval'=>1
      );
      $model=CourseRegister::model()->findAll($criteria);
      return count($model);
      }
      public static function  checkmax2($id) { // นับทั้งหมดคนที่ยังไม่อณุมัติด้วย
      $criteria = new CDbCriteria ();
      $criteria->condition = 'course_id=:cu_id';
      $criteria->addCondition('approval=:approval', 'AND');
      $criteria->params = array(
      ':cu_id' =>$id,':approval'=>1
      );
      $model=  self::model()->findAll($criteria);
      return count($model);
      } */

    /*
      public static function  checuserregisters($courseid,$user) {
      $criteria = new CDbCriteria ();
      $criteria->condition = 'course_id=:course AND employee_id=:user';
      $criteria->addInCondition('approval', array('1', '2'), 'AND');
      $criteria->params = array(
      ':course' =>$courseid,'user'=>$user
      );
      $model=self::model()->findAll($criteria);
      return count($model)>0;// return true ถ้าลงทะเีบยนไปแล้ว
      } */

    public static function getstatus($status) {
        if ($status == 2) {
            return "ไม่อณุมัติ";
        } else if ($status == 1) {
            return "อณุมัติแล้ว";
        } else if ($status == 0) {
            return "กำลังดำเนินการ";
        } else if ($status == 3) {
            return "กรุณาทำแบบประเมิน";
        } else if ($status == 4) {
            return "ผ่านการอบรมแล้วค่ะ";
        }
        return "ไม่รู้จัก";
    }
 public function checkexites_course($attribute,$params){ 
     $iscourse=Course::iscourse($this->attributes['course_id']);
     $isexpirecourse = Course::isexpirecourse($this->attributes['course_id']);
      if ($iscourse) {//มีคอร์ดอยู่เหรอไม่
         if (!$isexpirecourse){// หลักสูตรปิดไปแล้วค่ะ
               $this->addError($attribute, 'หลักสูตรปิดไปแล้วค่ะ');  
          } 
        } else {// ไม่พบข้อมูลหลักสูตร 
          $this->addError($attribute, 'ไม่พบข้อมูลหลักสูตร');  
        } 
   }
   public static function getUserRegister_textimplod($id) {
        $arrayuser=array();
        $userretext='';
        $userregis = Yii::app()->db->createCommand()
                ->select('employee_id')
                ->from('course_register')
                ->where('course_id=:course_id',array(':course_id'=>$id))
                ->queryAll();
        foreach ($userregis as $key => $value) {
            array_push($arrayuser, $value['employee_id']); 
        }
        return implode(',', $arrayuser);
   }
   public static function ischeck_user_freetime($userid,$timestar,$timeends) {
       $sql="select count(*) as mycount from course_register where  EXISTS (select 1 from daycoursetraining dt WHERE employee_id='".$userid."' AND course_id=dt.idcourse AND dt.day BETWEEN '".$timestar."' AND '".$timeends."');"; 
       $dbCommand = Yii::app()->db->createCommand($sql);
       $data=$dbCommand->queryAll();
      // var_dump($data[0]['mycount']);exit();
      return $data[0]['mycount']>0;
   }
    protected function beforeSave() {
        if ($this->isNewRecord)
            $this->time = new CDbExpression('NOW()');
        return parent::beforeSave();
    }

    public function getdaynow() {
        return $this->daynow = date('Y-m-d');
    }

    protected function afterSave() {
        if ($this->isNewRecord) {
            $this->isNewRecord = false;
            $dateId = 'CR' . date("Y") . date("m") . date("d") . sprintf("%04d", $this->id);
            $this->saveAttributes(array('idcourse' => $dateId));
            //  $this->saveAttributes(array('time' =>new CDbExpression('NOW()'));
            $this->isNewRecord = true;
        }
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
