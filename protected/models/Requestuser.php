<?php

class Requestuser extends CActiveRecord {

    public function tableName() {
        return 'requestuser';
    }

    public function rules() {
        return array(
            //  array('idcourse', 'unique'),
            array('todapt,num', 'required'),
            array('num', 'numerical', 'integerOnly' => true),
            array('num','checkmax_user'),
            array('todapt', 'length', 'max' => 45),
            array('idcourse', 'length', 'max' => 100),
            array('note', 'safe'),
            array('id, todapt, num, idcourse, note', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'todapt' => 'แผนก',
            'num' => 'จำนวน/คน',
            'idcourse' => 'รหัสคอร์ส',
            'note' => 'หมายเหตุ',
        );
    }

    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('todapt', $this->todapt, true);
        $criteria->compare('num', $this->num);
        $criteria->compare('idcourse', $this->idcourse, true);
        $criteria->compare('note', $this->note, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
 public function checkmax_user($attribute,$params){ 
    $mynum=0;
    /*
      $criteria = new CDbCriteria ();
        $criteria->condition = 'idcourse=:cu_id';
     //   $criteria->addCondition('approval=:approval', 'AND');
         $criteria->params = array(
             ':cu_id' =>$this->attributes['idcourse']//,':approval'=>1
         );
        $model=$this->model()->findAll($criteria);     
      */ 
    $mynum=self::getnumrequest($this->attributes['idcourse']);
    $max_Numberof = Course::model()->getmax_Numberof($this->attributes['idcourse']);
    $numregis = CourseRegister::model()->checkRegister($this->attributes['idcourse']);
    $isfull =($numregis+$mynum+$this->attributes['num'])>$max_Numberof; // true ถ้าไม่เต็ม
    $emplo=Employee::getnumoffdepartments($this->attributes['todapt']);
    //var_dump($mynum);
	$checktrue=true;
	if($this->attributes['num']>$emplo){
	 $this->addError($attribute, 'แผนก'.Department::getlabeldepartmanets($this->attributes['todapt']).'มีพนักงาน'.$emplo.'  คนเท่านั้น');  
	$checktrue=false;
	}else if($checktrue && $isfull){
		$i=$max_Numberof-($numregis+$mynum);
      //  $this->addError($attribute, 'คงเหลือ "'.$i.'"คนเท่านั้น');  
	$this->addError($attribute, 'แผนก'.Department::getlabeldepartmanets($this->attributes['todapt']).'มีพนักงานที่สามารถสมัครได้  '.$i.' คนเท่านั้น'); 	
	}  
   }
   public static function getnumrequest($idcourse) {
    $sql='SELECT sum(num)as mynum FROM requestuser WHERE  idcourse="'.$idcourse.'";';
    $dbCommand = Yii::app()->db->createCommand($sql);
    $data=$dbCommand->queryAll();
    if(count($data)>0){
        return  $data[0]['mynum'];    
    }
   return  0;    
   }
}
