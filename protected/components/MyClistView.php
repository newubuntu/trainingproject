<?php
Yii::import('ext.yiibooster.widgets.TbListView');
class MyClistView extends TbListView{
 public function renderSorter(){
        $this->pager = array('class'=>'MyPager');
 
  if($this->dataProvider->getItemCount()<=0 || !$this->enableSorting || empty($this->sortableAttributes))
            return;
        echo CHtml::openTag('div',array('class'=>$this->sorterCssClass))."\n";
        echo $this->sorterHeader===null ? Yii::t('zii','Sort by: ') : $this->sorterHeader;
        echo "<ul>\n";
        $sort=$this->dataProvider->getSort();
        foreach($this->sortableAttributes as $name=>$label)
        {
            if($label == 'created_at') {
                $link_name = "<span>по дате добавления</span>";
            } else {
                $link_name = "<span>по рейтингу</span>";
            }
 
            if(is_integer($name)){
 
                //если производят сортировку
                if(!empty($_GET['Testimonial_sort'])) {
 
                    $sorter_type = $_GET['Testimonial_sort'];
 
                    if($sorter_type == 'created_at' && $name == 1) {
                        echo $sort->link($label, $link_name.' ▼', $htmlOptions=array('class'=>'active'));
 
                    } elseif($sorter_type == 'created_at.desc' && $name == 1) {
                        echo $sort->link($label, $link_name.'  ▲', $htmlOptions=array('class'=>'active'));
 
                    } elseif($sorter_type == 'rating' && $name == 0) {
                        echo $sort->link($label, ''.$link_name.'  ▼', $htmlOptions=array('class'=>'active'));
 
                    } elseif($sorter_type == 'rating.desc' && $name == 0) {
                        echo $sort->link($label, $link_name.' ▲', $htmlOptions=array('class'=>'active'));
 
                    } else {
                        echo $sort->link($label, $link_name);
                    }
 
 
                } else {
                    if($label == 'created_at') {
                        echo $sort->link($label, $link_name.' ▲', $htmlOptions=array('class'=>'active'));
                    } else {
                        echo $sort->link($label, $link_name);
                    }
                }
 
            } else {
                echo $sort->link($name,$label);
            }
        }
        echo "</ul>";
        echo $this->sorterFooter;
        echo CHtml::closeTag('div');
    } 
}