<?php

class TrainingHistoryController extends Controller {
    public $layout = '//layouts/column2';
    public function filters() {
        return array(
            'accessControl',  
        );
    }
    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('Admin','Delete','Ajaxupdate'),
                'users' => array('1'), // 
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModelByattru($id);  
            $oldmodel=$model;
            if ($model->delete()) { 
              $criteria = new CDbCriteria;
              $criteria->addInCondition('course_id',array($oldmodel->cu_id));
              CourseRegister::model()->deleteAll($criteria);   
             echo "1";exit();   
         }else{
             echo "2";exit();    
         }   
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionAdmin() {
        $model = new TrainingHistory('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['TrainingHistory']))
            $model->attributes = $_GET['TrainingHistory'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = TrainingHistory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    public function loadModelByattru($id) {
        $model = TrainingHistory::model()->findByAttributes(array('id'=>$id));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    } 
  public function loadModelByattrucu_id($id) {
        $model = TrainingHistory::model()->findByAttributes(array('cu_id'=>$id));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }   
    public function actionAjaxupdate() { 
        $autoIdAll = $_POST['cu_id']; 
        if (count($autoIdAll) > 0) {
            foreach ($autoIdAll as $autoId) { 
               $model = $this->loadModelByattrucu_id($autoId); 
                $oldmodel=$model;         
               if ($model->delete()){ 
                     $criteria = new CDbCriteria;
                     $criteria->addInCondition('cu_id',array($oldmodel->cu_id));
                     CourseRegister::model()->deleteAll($criteria); 
                } else {
                    throw new Exception("Sorry", 500);
                }
            }
        }
    }
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'training-history-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
