<?php
class CourseRegisterController extends Controller {
    public $layout = '//layouts/column2';
    public function filters() {
        return array(
            'accessControl',  
        );
    }
    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('registercouseDetail','Delete','UpdateUserDetail','Noapproval','NoapprovalDetail','registercouseAll'),
                'users' => array('1'), // 
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actionregistercouseAll() { 
         $model = new CourseRegister('listcoursregister');
          $model->unsetAttributes();  // clear any default values 
           if (isset($_GET['CourseRegister']))
            $model->attributes = $_GET['CourseRegister'];
            $this->render('registercouseAll', array(
            'model' => $model,
        )); 
    }
    public function actionregistercouseDetail($id) { 
         $departments=NULL;
         if(!Yii::app()->user->isAdmin()){
          $departments=Yii::app()->user->getdepartments();   
         }  
        $criteria = new CDbCriteria; 
        $criteria->together = true;
        $criteria->with = array('course');
        $criteria->together = true;
        $criteria->with = array('employee');
        $criteria->compare('course_id',$id);
        $criteria->compare('employee.iddept', $departments);
        $criteria->addInCondition('approval', array('0','1','2'), 'AND');
        //$criteria->group = 'employee_id';
        $dp = new CActiveDataProvider('CourseRegister', array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    'iddept',
                ),
                'defaultOrder' => 'iddept',
            ),
            'pagination' => array(
                'pagesize' => 10,
            ),
        )); 
        $this->render('registercouse', array('dp' => $dp));
    } 
    public function actionUpdateUserDetail($cid,$emid) {
        $criteria = new CDbCriteria;
        $criteria->together = true;
        $criteria->with = array('course','employee');
        $criteria->compare('course_id',$cid); 
        $criteria->compare('employee_id',$emid);  
        $model=CourseRegister::model()->find($criteria);
         if($model==Null){
           throw new CHttpException(404, 'The requested page does not exist.');    
         }else{
       // $sql = 'SELECT sum(score) as score FROM training_usershistory WHERE employee_id="' . $userid . '" AND cu_id="' . $idcourse . '";';
       // $dbCommand = Yii::app()->db->createCommand($sql);
      //  $data = $dbCommand->queryAll();
         }  
        $criteria2 = new CDbCriteria ();
        $criteria2->condition = 'course_id=:cu_id';
        $criteria2->addCondition('employee_id=:employee_id','AND');
        $criteria2->params = array(
            ':cu_id' => $cid,':employee_id'=>$emid
        ); 
        $modelregis= CourseRegister::model()->find($criteria2);
        $modelregis->setScenario('update');
        $this->performAjaxValidation($modelregis);
         if (isset($_POST['CourseRegister'])) {// save update data
            $modelregis->attributes = $_POST['CourseRegister']; 
            if($modelregis->save()){ 
                 Yii::app()->user->setFlash("success", "บันทึกข้อมูลเรียบร้อยแล้วค่ะ");  
            }else {
                Msg::error($model->getErrors());
                Yii::app()->user->setFlash("error", "ไม่สามารถบันทึกข้อมูลได้ค่ะ");
            }
         } 
         $this->render('UpdateUser',array('model'=>$model,'modelregis'=>$modelregis));
    }
    
    public function actionNoapproval() {
          $model = new CourseRegister('listcourse_Noapproval');
          $model->unsetAttributes();  // clear any default values 
           if (isset($_GET['CourseRegister']))
            $model->attributes = $_GET['CourseRegister'];
            $this->render('Noapproval', array(
            'model' => $model,
        )); 
    }
    public function actionNoapprovalDetail($id) {
          $model = new CourseRegister('ListNoapprovaDetail');
          $model->unsetAttributes();  // clear any default values 
           if (isset($_GET['CourseRegister']))
            $model->attributes = $_GET['CourseRegister']; 
        $this->render('NoapprovalDetail', array('model' => $model));
    }
    public function actionDelete($rcid) {
        if (Yii::app()->request->isPostRequest) { 
            $model = $this->loadModelByattbut($rcid);   
            if($model->delete()){ 
             echo "1";exit();   
            }else{
            echo "2";exit();      
            } 
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else 
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    
    public function loadModel($id) {
        $model = CourseRegister::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    public function loadModelByattbut($id) {
        $model = CourseRegister::model()->findByAttributes(array('idcourse'=>$id));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'course-register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
