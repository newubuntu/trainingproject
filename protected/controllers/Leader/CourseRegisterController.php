<?php
class CourseRegisterController extends Controller {
    public $layout = '//layouts/column2';
    public function filters() {
        return array(
            'accessControl',  
        );
    }
    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('registercouseDetail','registercouseAll'),
                'users' => array('2'), // 
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actionregistercouseAll() { 
         $model = new CourseRegister('listcoursregister');
          $model->unsetAttributes();  // clear any default values 
           if (isset($_GET['CourseRegister']))
            $model->attributes = $_GET['CourseRegister'];
            $this->render('registercouseAll', array(
            'model' => $model,
        )); 
    }
    public function actionregistercouseDetail($id) { 
         $departments=NULL;
         if(!Yii::app()->user->isAdmin()){
          $departments=Yii::app()->user->getdepartments();   
         }  
        $criteria = new CDbCriteria; 
        $criteria->together = true;
        $criteria->with = array('course');
        $criteria->together = true;
        $criteria->with = array('employee');
        $criteria->compare('course_id',$id);
        $criteria->compare('employee.iddept', $departments);
        $criteria->addInCondition('approval', array('0','1','2'), 'AND');
        $criteria->group = 'employee_id';
        $dp = new CActiveDataProvider('CourseRegister', array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    'iddept',
                ),
                'defaultOrder' => 'iddept',
            ),
            'pagination' => array(
                'pagesize' => 10,
            ),
        )); 
        $this->render('registercouse', array('dp' => $dp));
    }  
    
    public function loadModel($id) {
        $model = CourseRegister::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'course-register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
