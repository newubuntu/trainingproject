﻿<div class="row">
    <div class="col-xs-12">
        <div id="main_boxslide">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <!--- div profiles--->
                            <div id='mainbox_profile'>
                                <div style="margin-left:5px;margin-right:5px;margin-top: 7px;">
                                    <div class="row clearfix">
                                        <div class="col-xs-12">
                                            <div class="panel panel-default">
                                                <!-- Default panel contents -->
                                                <div class="panel-heading">
                                                    <div class="course-sub">
                                                        <h4 style="padding:5px 15px 3px 20px; background:#428bca; border-radius:5px; margin-button:5px; margin-top:5px; color:#ffffff;"><span class="glyphicon glyphicon-play">&nbsp;</span>รายการหลักสูตรการอบรม</h4>	
                                                    </div>
                                                </div>

                                                <!-- List group -->
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th style="background:#5F9CCA; color:#fff; width:50px;">ลำดับที่</th>
                                                            <th style="background:#5F9CCA; color:#fff; width:150px;">รหัสหลักสูตร</th>
                                                            <th style="background:#5F9CCA; color:#fff; width:250px;">ชื่อหลักสูตร</th>
                                                            <th style="background:#5F9CCA; color:#fff; width:100px;">วันเปิดรับสมัคร</th>
                                                            <th style="background:#5F9CCA; color:#fff; width:100px;">วันปิดรับสมัคร</th>
                                                            <th style="background:#5F9CCA; color:#fff; width:100px;">สมัคร/รับสมัคร</th>
                                                            <th style="background:#5F9CCA; color:#fff; width:100px;">สถานะ</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $numuserall=0;
                                                        $num = 0;
                                                        if (isset($_GET['page'])) {
                                                            $num = ($_GET['page'] - 1) * Yii::app()->params['pagessize'];
                                                        }
                                                        foreach ($modelhistory as $key2 => $value2) {
                                                            $num++;
														    
                                                            $max_Numberof = Course::model()->getmax_Numberof($value2->cu_id);
															$mynum=Requestuser::getnumrequest($value2->cu_id);
                                                            $numregis = CourseRegister::model()->checkRegister($value2->cu_id);
                                                           // $isfull =CourseRegister::isfullCourse($value2->cu_id);  // true ถ้าไม่เต็ม
															$isfull =($numregis+$mynum+1)>$max_Numberof; // true ถ้าไม่เต็ม
                                                            $isexpirecourse = Course::isexpirecourse($value2->cu_id);
                                                            //echo 'numregis'.$numregis.'counchk'.$counchk[0]->num_max; 
                                                            if ($isexpirecourse) {// ยังเปิด
                                                                if (!$isfull) {// ไม่เต็ม 
                                                                    $pcen = $max_Numberof - ($max_Numberof-($numregis+$mynum)) . "/" . $max_Numberof;
                                                                    $path = Yii::app()->createUrl('course_detail&id=') . $value2->cu_id;
                                                                    $alink = "<a href=\"$path\">" . $value2->cu_id . "</a>";
                                                                    $alinlname = "<a href=\"$path\">" . cutword::substr_utf8($value2->name, 0, 30) . "</a>";
                                                                    $iscloses = "<font color=\"WHITE\"><a href=\"$path\" class=\"btn btn-success btn-sm active\" role=\"button\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span>&nbsp;รับสมัคร</a></font>";
                                                                } else { // เต็ม
                                                                    $iscloses = "<font color=\"red\">ผู้สมัครเต็มแล้ว</font>";
                                                                    $pcen = $max_Numberof - ($max_Numberof-($numregis+$mynum)) . "/" . $max_Numberof;
                                                                    $alink = "<span style='color:red;'>" . $value2->cu_id . "</span>";
                                                                    $alinlname = "<span style='color:red;'>" . cutword::substr_utf8($value2->name, 0, 30) . "</span>";
                                                                }
                                                            } else {// ปิดไปแล้ว
                                                                //$numuserall=  TrainingUsershistory::get_numalluser($value2->cu_id);
                                                                $iscloses = "<font color=\"red\">ปิดรับสมัคร</font>";
                                                                $pcen = $max_Numberof - ($max_Numberof-  TrainingUsershistory::get_numalluser_complete($value2->cu_id)) . "/" . $max_Numberof;
                                                                $alink = "<span style='color:red;'>" . $value2->cu_id . "</span>";
                                                                $alinlname = "<span style='color:red;'>" . cutword::substr_utf8($value2->name, 0, 30) . "</span>";
                                                            }
                                                            ?>
                                                            <tr class="odd">
                                                                <td style="text-align: center;width: 10px"><?= $num ?></td>
                                                                <td style="text-align: center;width: 10px"><?= $alink ?></td>
                                                                <td style="width: 40px"><?= $alinlname ?></td>
                                                                <td style="text-align: right;width: 10px"><?= $value2['dayopencoure'] ?></td>
                                                                <td style="text-align: right;width: 10px"><?= $value2['dayclose'] ?></td>
                                                                <td style="text-align: center;width: 10px"><?= $pcen ?></td>
                                                                <td style="text-align: center;width: 10px"><?= $iscloses ?></td>

                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table> 
                                            </div>   
                                            <div class="boxpagination"> 
                                                <?php
                                                $this->widget('CLinkPager', array(
                                                    'pages' => $pages,
                                                    'header' => '',
                                                    'prevPageLabel' => '&laquo; Next',
                                                    'nextPageLabel' => 'Prev &raquo;',
                                                    'selectedPageCssClass' => 'active',
                                                    'hiddenPageCssClass' => 'disabled',
                                                    'maxButtonCount' => 10,
                                                    'htmlOptions' => array(
                                                        'class' => 'pagination'
                                                    ),
                                                ));
                                                ?>
                                            </div>
                                        </div> 
                                    </div>

                                </div>
                                <div style="clear:both;"></div>		  
                            </div> 
                        </div>
                    </div><!-- /.row -->
                </div>
            </div>
        </div> 
    </div>
</div>  
</div>