<style type='text/css'>
    /* grid border */
    .grid-view table.items th, .grid-view table.items td {
        border: 1px solid gray !important;
    } 

    /* disable selected for merged cells */     
    .grid-view td.merge {
        background: none repeat scroll 0 0 #F8F8F8; 
    }
</style>
<div class="well"> 
    <h3>รายชื่อผู้สมัครทั้งหมด</h3>
    <?php
    $this->widget('ext.groupgridview.GroupGridView', array(
        'id' => 'grid1',
        'dataProvider' => $dp,
        // 'itemsCssClass'=>'table table-bordered table-hover',
        // 'rowCssClassExpression'=>'($data->approval==4)?"info":($data->approval==1?"success":$data->approval==2?"danger":"")',
        'mergeColumns' => array('employee.iddept'),
        'columns' => array(
            array(
                'name' => 'employee.iddept',
                'header' => '<span style="color:#white;text-align:center;">แผนก</span>',
                'value' => 'Department::getlabeldepartmanets($data->employee->iddept)',
            ),
            'employee.firstname',
            'employee.lastname',
            array( 
                'name' => 'approval', 
                'value' => 'CourseRegister::getstatus($data->approval)',
            ),
        ),
    ));
    ?>
</div>