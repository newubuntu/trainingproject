<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('course-register-grid', {
data: $(this).serialize()
});
return false;
});
");
?>
<style type="text/css">
    .text_center{
        text-align:center;
    }
</style>
<div class="well"> 
    <?php // echo CHtml::link('Advanced Search', '#', array('class' => 'search-button btn btn-primary')); ?>
    <div class="search-form" style="display:none">
        <?php
        //$this->renderPartial('_search', array(
        //    'model' => $model,
        // ));
        ?>
    </div><!-- search-form -->
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'enableAjaxValidation' => FALSE,
    ));
    ?>
    <div style="margin-bottom:-46px;;" class="well">
        <h3>รายการหลักสูตรที่มีผู้สมัครเรียน</h3>
    </div>

    <?php
         $departments=NULL;
         if(!Yii::app()->user->isAdmin()){
          $departments=Yii::app()->user->getdepartments();   
         }  
       $this->widget('booster.widgets.TbGridView', array(
        'id' => 'course-register-grid',
        'dataProvider' =>$model->listcoursregister($departments),
        'type' => 'striped bordered',
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'course_id',
                'htmlOptions' => array('style' => 'width: 150px'),
            ),
            array(
                'name' => 'coursename',
                'header' => 'ชื่อหลักสุตร',
                'value' => '$data->course->name',
            ),
            array(
                'header' => '<span style="color:#428bca;text-align:center;">ประเภทหลักสูตร</span>',
                'htmlOptions' => array('class' => 'form-control'),
                'filter' => CHtml::dropDownList('CourseRegister[r_categorycourse]','ไม่รู้อัไร', CHtml::listData(Categorycourse::getTypescourse(), 'id', 'name'), array('empty' => '-- หลักสูตร --', 'class' => 'form-control',)
                ),
                'value' => 'Categorycourse::getlabelTypescourse($data->course->categorycourse)',
                'htmlOptions' => array(
                    'style' => 'width: 180px;text-align:center;'//, 'class' => 'form-control'
                ),
            ),
            array('header' => '<span style="color:#428bca;">ข้อมูล</span>',
                'class' => 'booster.widgets.TbButtonColumn',
                'template' => '{info}', //    'template'=>'{add} {list} {update} {print_act}',
                'buttons' => array(
                    'info' => array(
                        'label' => 'ดูข้อมูลการสมัคร',
                        'icon' => 'fa fa-bar-chart-o',
                        'url' => 'Yii::app()->controller->createUrl("admin/courseRegister/registercouseDetail", array("id"=>$data->course_id))',
                        'options' => array(
                            'class' => 'btn btn-small btn-success', 'style' => 'margin:5px;',
                        ),
                    ),
                ),
                'htmlOptions' => array(
                    'style' => 'width: 150px;', 'class' => 'text_center'
                ),
            ),
        ),
      )
    );
    ?>
</div>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('course-register-grid');
    }
</script>
<?php $this->endWidget(); ?>
 
