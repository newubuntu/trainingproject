<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile($baseUrl . '/datepicker/js/bootstrap-datepicker.js');
//$cs->registerScriptFile($baseUrl . '/datepicker/js/bootstrap-timepicker.min.js');
$cs->registerScriptFile($baseUrl . '/bootstrap_datetimepicker/js/moment.js');
$cs->registerScriptFile($baseUrl . '/bootstrap_datetimepicker/js/bootstrap-datetimepicker.min.js');
$cs->registerCssFile($baseUrl . '/bootstrap_datetimepicker/css/bootstrap-datetimepicker.min.css');
//$cs->registerCssFile($baseUrl . '/datepicker/css/bootstrap-datetimepicker.min.css');
$stafarray = array();
$training_array = array();
?>

<script type="text/javascript">
    var training_array = [];
    var stafarray = [];
    var iclick = true, iclick2 = true;
    var isinitvar = true;
    var ismyclick = true;
    var isstafclick = true;
    $(document).ready(function () {
        if (isinitvar) {
            intivailible_sever();
            isinitvar = false;
        }
        $("#optionssupprier1").click(function () {
            $('#boxisupprier').fadeOut('fast');
            $("#boxonisupprier").slideDown("slow", function () {
            });
        });
        $("#optionssupprier2").click(function () {
            $('#boxonisupprier').fadeOut('fast');
            $("#boxisupprier").slideDown("slow", function () {
            });
        });
        $("#addButton_training").click(function () {
            if (getdaynum() >= 20) {
                alert("มากสุด 20");
                return false;
            }
            if (chkexitesday() == 2) {
                swal('Error...!', 'ข้อมูลวันที่ไม่ถูกต้องค่ะ!', 'error');
                return false;
            } else if (chkexitesday() == 3) {
                swal('Error...!', 'วันที่เปิดรับสมัครหลักสูตรไม่ควรมากกว่าวันที่ปิดรับสมัครหลักสูตรค่ะ!', 'error');
                return false;
            } else if (chkexitesday() == 4) {
                 swal('Error...!', 'ข้อมูลวันที่ซ้ำค่ะ!', 'error');
                return false;
            }
            var newTextBoxDiv = $(document.createElement('div'))
                    .attr("id", 'TextBoxDiv_training' + (getdaynum() + 1));
            newTextBoxDiv.after().html('<table cellpadding="0" cellspacing="0" style="margin: 0 auto 0 10%;" border="0">' +
                    '<tr>' +
                    '<td>วันที่ ' + (getdaynum() + 1) + '</td>' +
                    '<td><div class="form-group">' +
                    '<div class="input-group"><span class="input-group-addon">' +
                    '<i class="glyphicon glyphicon-calendar"></i></span>' +
                    '<input style="width:100px;" id="mydaymore" class="form-control ct-form-control mydaymore" type="text"' +
                    'placeholder="2014-01-01" name="day[]" id="">' +
                    '</div></div></td>' +
                    '<td>เวลา</td>' +
                    '<td> <div class="form-group">' +
                    '<div class="input-group date form_time col-md-8" class="datetimepicker_start">' +
                    '<input style="width:100px;"  id="mydayfirsttime" class="form-control mydayfirsttime" type="text" placeholder="12:00" name="timestart[]" value="">' +
                    '<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>' +
                    '</div></div></td><td>ถึง</td>' +
                    '<td> <div class="form-group">' +
                    '<div class="input-group date form_time col-md-8" data-date="" data-date-format="hh:ii">' +
                    '<input style="width:100px;" id="mydayfirsttime" class="form-control mydayendtime" type="text" placeholder="12:00" name="timeend[]" value="">' +
                    '<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>' +
                    '</div> </div></td></tr><tr>' +
                    '<td colspan="6">' +
                    '<textarea rows="3" class="form-control detailTraining" placeholder="รายละเอียดการสอน" name="detailTraining[]"></textarea>' +
                    '</td> </tr></table>');
            newTextBoxDiv.fadeIn("slow").appendTo("#TextBoxesGroup_training");
            add_removedaytraining('add');
        });
        $("#removeButton_training").click(function () {
            if (getdaynum() == 0) {
                swal({allowOutsideClick: true, title: "Error!", text: 'ไม่มีข้อมูลที่ลบ!', type: "error"});
                return false;
            }
            $("#TextBoxDiv_training" + getdaynum()).fadeOut(300, function () {
                $(this).remove();
                add_removedaytraining(''); //remove
            });
        });
        /*################################### */
        $('#addButton_training_staf').click(function () {
        });
        $("#removeButton_training_staf").click(function () {
            if (getnumuser() == 0) {
                swal({allowOutsideClick: true, title: "Error!", text: 'ไม่มีข้อมูลที่ลบ!', type: "error"});
                return false;
            }
            $("#TextBoxDiv_training_staf" + getnumuser()).fadeOut(300, function () {
                $(this).remove();
                add_removeuserserver('', stafarray.length - 1); //remove
            });
        });
    });
    function mydateDiff(str1, str2) {
        var diff = Date.parse(str2) - Date.parse(str1);
        return isNaN(diff) ? NaN : {
            diff: diff,
            ms: Math.ceil(diff % 1000),
            s: Math.ceil(diff / 1000 % 60),
            m: Math.ceil(diff / 60000 % 60),
            h: Math.ceil(diff / 3600000 % 24),
            d: Math.ceil(diff / 86400000)
        };
    }
    function parseDate(str) {
        var mdy = str.split('-');
        return new Date(mdy[2], mdy[1] - 1, mdy[0]);
    }
    function daydiff(first, second) {
        return (second - first) / (1000 * 60 * 60 * 24);
    }
    function selectDataemployee(id, name) {
        if (confirm("คุณเลือก พนักงาน  " + name)) {
            createemployee(id);
            // $("#Course_supprier_id").val(id);
            // $("#isupprier_id").val(id);
            $('#employeeModal').modal("hide");
        }
        // $("#data-info .info").html(name);
    }
    function createemployee(id) {
        if (getnumuser() > 20) {
            alert("มากสุด 20");
            return false;
        }
        var addtrue = seachdep(id, 'staf');
        if (addtrue) {
            swal('Error...!', 'ข้อมูลซ้ำค่ะ!', 'error');
            $('#myModal').modal("hide");
            return false;
        }
        var newTextBoxDiv = $(document.createElement('div'))
                .attr("id", 'TextBoxDiv_training_staf' + (getnumuser() + 1));
        newTextBoxDiv.after().html('<div class="form-group">' +
                '<label class="col-sm-3 control-label">ผู้ดูแลคอร์สคนที่ ' + (getnumuser() + 1) + '</label>' +
                '<div class="col-sm-9"><input style="width:250px;" class="form-control my_id_stafclass" placeholder="ผู้ดูแลคอร์ส" id="my_id_staf' + (getnumuser() + 1) + '" value="' + id + '"name="id_staf[]" type="text">' +
                '</div></div>');
        newTextBoxDiv.fadeIn("slow").appendTo("#TextBoxesGroup_training_staf");
        add_removeuserserver('add', id);
    }
    function intivailible_sever() {
        var userserver = $('#userserver').val();
        var daytrainingserver = $('#training_array').val();
        var spliteuserserver = userserver.split(',');
        var splitdayserver = daytrainingserver.split(',');
        var loop1 = spliteuserserver.length;
        var loop2 = splitdayserver.length;
        for (i = 0; i < loop1; i++) {
            if (!seachdep(spliteuserserver[i], 'staf') && spliteuserserver[i] != '') {
                stafarray.push(spliteuserserver[i]);
            }
        }
        for (j = 0; j < loop2; j++) {
            if (!seachdep(splitdayserver[j], 'day') && splitdayserver[j] != '') {
                training_array.push(splitdayserver[j]);
            }
        }
        var userserver = $('#userserver').val('');
        var daytrainingserver = $('#training_array').val('');
    }
    function getnumuser() {
        var mynum = 0;
        $('.my_id_stafclass').each(function () {
            mynum++;
        });
        return mynum;
    }
    function getdaynum() {
        var mynum = 0;
        $('.mydaymore').each(function () {
            mynum++;
        });
        return mynum;
    }
    function chkexitesday() {
        var arraychk = [], num = 0;
        var myarraychk = [];
        var chk = 1;
        var aa = true;
        var Course_dayclose = $('#Course_dayclose').val();
        $('.mydaymore').each(function () {
            arraychk.push($(this).val());
        });
        var looparraychk = arraychk.length;
        for (j = 0; j < looparraychk; j++) {
            if (isValidDate(arraychk[j])) {
                myarraychk.push(arraychk[j]);
            }
        } 
        for (k = 0; k < looparraychk; k++) { 
            if (!isValidDate(arraychk[k])) {
                chk = 2; 
            }
        }
        var loop = myarraychk.length;
        if (chk != 2){
            for (i = 0; i < loop; i++) {
                if ((new Date(myarraychk[i]).getTime() < new Date(Course_dayclose).getTime())) {
                    chk = 3;
                    aa = false;
                } else if (arraychk.myUnique() && aa) {
                    chk = 4;
                    aa = false;
                }
            }
        }

        return  chk;

    }
    function add_removeuserserver(type, key) {//key เป็นได้ทั้ง key ที่ใช้ลบได้
        if (type == 'add') {
            if (!seachdep(key, 'staf')) {
                stafarray.push(key);
            }
        } else {
            if (stafarray.length > 0) {
                stafarray.remove(key);
            }
        }
    }
    function add_removedaytraining(type) {//key เป็นได้ทั้ง key ที่ใช้ลบได้
        var num = 0;
        var check = false; 
        if (type == 'add') {
            $('.mydaymore').each(function () {
                if (!isValidDate($(this).val())) {
                    check = true;
                    return false;
                } else {
                    if (!seachdep($(this).val(), 'day')) {
                        training_array.push($(this).val());

                    }
                }
            });
            if (check) {
                // swal({allowOutsideClick:true,title:"Error!",text: 'วันที่เปิดรับสมัครหลักสูตรไม่ควรเป็นค่าว่าง!', type:"error"});   
                return false;
            }
        } else {
            if (training_array.length > 0) {
                var l = training_array.arrgetlast();
                training_array.myremove(l);
            }
        }
    }
    function seachdep(key, type) {
        if (type == 'staf') {
            var loop = stafarray.length;
            for (i = 0; i < loop; i++) {
                if (stafarray[i] == key) {
                    return true;
                }
            }
        } else if (type == 'day') {
            var loop = training_array.length;
            for (i = 0; i < loop; i++) {
                if (training_array[i] == key) {
                    return true;
                }
            }
        } else {
            alert('error key seach !');
        }
    }
    function isValidDate(str) {
        // STRING FORMAT yyyy-mm-dd
        if (str == "" || str == null) {
            return false;
        }
        // m[1] is year 'YYYY' * m[2] is month 'MM' * m[3] is day 'DD'					
        var m = str.match(/(\d{4})-(\d{2})-(\d{2})/);
        // STR IS NOT FIT m IS NOT OBJECT
        if (m === null || typeof m !== 'object') {
            return false;
        }
        // CHECK m TYPE
        if (typeof m !== 'object' && m !== null && m.size !== 3) {
            return false;
        }
        var ret = true; //RETURN VALUE						
        var thisYear = new Date().getFullYear() + 1; //YEAR NOW
        var minYear = 1999; //MIN YEAR
        // YEAR CHECK
        if ((m[1].length < 4) || m[1] < minYear || m[1] > thisYear) {
            ret = false;
        }
        // MONTH CHECK			
        if ((m[1].length < 2) || m[2] < 1 || m[2] > 12) {
            ret = false;
        }
        // DAY CHECK
        if ((m[1].length < 2) || m[3] < 1 || m[3] > 31) {
            ret = false;
        }
        return ret;
    }
</script>
<script type="text/javascript">
    function selectData(id, name) {
        var myconfirm = confirm("คุณเลือก supprier " + name);
        if (myconfirm) {
            $("#Course_supprier_id").val(id);
            $("#isupprier_id").val(id);
            $('#myModal').modal("hide");
        }
        // $("#data-info .info").html(name);
    }
    function gettDatainfo(id) {
        $.ajax({
            url: '<?= Yii::app()->createUrl('admin/course/getSupprier') ?>',
            type: 'POST',
            cache: false, data: {id: id},
            beforeSend: function () {
            },
            success: function (html) {
                $("#datasup").empty().html(html);
                $("#myModalinfo").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus + "ajaxloadcontent");
            }
        });
    }
    function gettDatainfoemployee(id) {
        $.ajax({
            url: '<?= Yii::app()->createUrl('admin/course/getSupprier') ?>',
            type: 'POST',
            cache: false, data: {id: id},
            beforeSend: function () {
            },
            success: function (html) {
                $("#dataemployee").empty().html(html);
                $("#myModalinfoemployee").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus + "ajaxloadcontent");
            }
        });
    }

</script>
<?php
Yii::app()->clientScript->registerScript('myuid_date', "
var mydaystartcouse='';
var mydayendcouse='';
var Course_dayopencoure='',Course_dayclose='';
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
$(function(){
var date = new Date();
        date.setDate(date.getDate() - 1);
        $('body').on('focus', '.mydayfirsttime,.mydayendtime', function () {
           $(this).datetimepicker({pickDate: false});
        });
        $('body').on('focus', '.mydaymore', function () {
          $(this).datepicker({startDate: date, autoclose: true});
  });
});
var checkin = jQuery('#Course_dayopencoure').datepicker({
    beforeShowDay: function (date) {
        return date.valueOf() >= now.valueOf();
    },
    'todayBtn':'true',
    'autoclose':'true',
    'todayHighlight':'true'
}).on('changeDate', function (ev) {
    if (ev.date.valueOf() > checkout.datepicker('getDate').valueOf() || !checkout.datepicker('getDate').valueOf()) {
        var newDate = new Date(ev.date);
        newDate.setDate(newDate.getDate() + 1);
        checkout.datepicker('update', newDate);
    }
    $('#Course_dayclose')[0].focus();
});
var checkout = $('#Course_dayclose').datepicker({
    beforeShowDay: function (date) {
        if (!checkin.datepicker('getDate').valueOf()) {
            return date.valueOf() >= new Date().valueOf(); 
        } else {
            return date.valueOf() >= checkin.datepicker('getDate').valueOf();
        }
    },
    'todayBtn':'true',
    'autoclose':'true',
    'todayHighlight':'true'
}).on('changeDate', function (ev) {
mydaystartcouse=$('#Course_dayopencoure').val();
mydayendcouse=$('#Course_dayclose').val();
//console.log(Date.parse($('#Course_dayopencoure').val())<Date.parse($('#Course_dayclose').val()));
var start = checkin.datepicker('getDate');
var end   = checkout.datepicker('getDate'); 
 if(!start || !end)
        return;
    var days = (end - start)/1000/60/60/24;
    days+=1;
  $('#Course_time').val(days);
  $('#timetotal').val(days);
});

function myAfterValidateFunction(form, data, hasError){	
   var mymsg='กรุณากรอกข้อมูลวันจัดอบรมให้ครบค่ะ';
   var empty = false,empty2 = false,empty3 = false,empty4 = false,empty5 = false;//input[name^=product]
  
       Course_dayopencoure=$('#Course_dayopencoure').val();
       Course_dayclose=$('#Course_dayclose').val();
      // console.log(typeof $('#my_id_staf1').val()==='undefined');
       if(Course_dayopencoure==''){
         $('#Course_dayopencoure').focus();        
        swal({allowOutsideClick:true,title:\"Error!\",text: 'วันที่เปิดรับสมัครหลักสูตรไม่ควรเป็นค่าว่าง!', type: \"error\"});
        return false;
       }else if(Course_dayclose==''){
        $('#Course_dayclose').focus();
        swal('Error...!', 'วันที่ปิดรับสมัครหลักสูตรไม่ควรเป็นค่าว่าง!', 'error');
        return false;
       }else if((new Date(Course_dayopencoure).getTime()>new Date(Course_dayclose).getTime())){
       $('#Course_dayopencoure').focus();
        swal('Error...!', 'วันที่เปิดรับสมัครหลักสูตรไม่ควรมากกว่าวันที่ปิดรับสมัครหลักสูตรค่ะ!', 'error');
         return false;
       }else if(typeof $('#my_id_staf1').val()==='undefined'){
         $('#addButton_training_staf').focus();
         swal('Error...!', 'ต้องกำหนดผู้ดูแลหนักสูตรอย่างน้อย 1 คนค่ะ!', 'error');
         return false;
       }else if($('#mydayfirst').val()=='' || $('#mydayfirsttime').val()=='' || $('#mydaytwottime').val()=='' || $('#mydaydetail').val()==''){
       // $('#mydayfirst').focus();
        swal('Error...!', 'กรอกข้อมูลวันจัดอบรมอย่างน้อย 1 วันค่ะ!', 'error');
         return false;
       }else{
        $('.detailTraining').each(function(){
          if($(this).val()==''){ empty5=true; }
         });
         if(empty5){
         $(this).focus();
          swal('Error...!', mymsg, 'error');
         return false;
          }
         $('.mydayfirsttime').each(function(){
	 if($(this).val()==''){ empty3=true; }
	});
	if(empty3){
        $(this).focus();
	 swal('Error...!', mymsg, 'error');
	return false;
	 }
          $('.mydayendttime').each(function(){
	 if($(this).val()==''){ empty4=true; }
	});
	if(empty4){
        $(this).focus();
	 swal('Error...!', mymsg, 'error');
	return false;
	 }
         
        $('.mydaymore').each(function(){
	 if($(this).val()==''){ empty2=true; }
	});
	if(empty2){
        $(this).focus();
	 swal('Error...!', mymsg, 'error');
	return false;
	 } 
        $('.mydaymore').each(function(){
	 if(Date.parse($(this).val())<=Date.parse(mydayendcouse)){ empty=true; }
	});
	if(empty){
        $(this).focus();
	 swal('Error...!', 'ข้อมูลวันจัดอบรมไม่ควรน้อยกว่าวันปิดรับสมัครค่ะ!', 'error');
	return false;
	 }
        }
      return true;
   }
", CClientScript::POS_READY);
?> 
<?php if (Yii::app()->user->hasFlash('error')): ?>
    <div class="alert in fade alert-danger">
    <?= Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php
    Yii::app()->clientScript->registerScript(
            'myalertEffect', "swal('Error...!', 'ไม่สามารถบันทึกข้อมูลได้ค่ะ!', 'error');", CClientScript::POS_READY
    );
    ?>
<?php endif; ?> 
<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'course-form',
    'enableAjaxValidation' => true,
    'type' => 'horizontal',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'beforeValidate' => 'js:myAfterValidateFunction'
    ),
    'htmlOptions' => array('class' => 'well', 'enctype' => 'multipart/form-data'
    )
        ));
?>
    <?php
    $mytrue = TRUE;
    if (!$model->isNewRecord) {
        $mytrue = FALSE;
    }
    ?>
<fieldset class="myfieldset">
    <?php
    if ($mytrue) {
        ?>
        <legend class="mylegend"><i class="glyphicon glyphicon-plus"></i>เพิ่มหลักสูตรเรียน</legend>
            <?php
        } else {
            ?>
        <legend class="mylegend"><i class="glyphicon glyphicon-pencil"></i>แก้ไขหลักสูตรเรียนรหัสส&nbsp;<?= $model->cu_id ?></legend>    
            <?php
        }
        ?>
    <div style="text-align: center;padding-right: 40%;">
        <p class="help-block">โปรดกรอกข้อมูลให้ครบทุกช่อง<span class="required">    *</span> </p>
    </div>
        <?php
        $mylist = Categorycourse::getTypescourse();
        $listcat = CHtml::listData($mylist, 'id', 'name');
// $Catlist = CHtml::listData(CategoryNews::getmenu(), 'cn_id', 'cn_name');
//var_dump($listcat);
        ?>
    <div class="well">
<?php echo $form->switchGroup($model, 'active', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:250px;')), array('events' => array('switchChange' => 'js:function(event, state){console.log(this);consold.log(event);console.log(state)}')))); ?>  
<?php echo $form->errorSummary($model); ?>
        <input type="hidden" name="timetotal" id="timetotal">
        <input type="hidden" name="pricecourse" id="pricecourse">
<?php echo $form->textFieldGroup($model, 'cu_id', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:250px;', 'disabled' => $mytrue)))); ?>
            <?php echo $form->textFieldGroup($model, 'name', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:250px;')))); ?>
            <?php //echo $form->radioButtonListGroup($model, 'categorycourse', array('widgetOptions' => array('data' => $listcat))); ?><!--'  คอมพิวเตอร์', ' จป.วิชาชีพ', ' ควบคุมคุณภาพ ISO', '  การตลาด', ' บัญชี', ' พนักงานใหม่'-->   
            <?php echo $form->dropDownListGroup($model, 'categorycourse', array('widgetOptions' => array('data' => $listcat, 'htmlOptions' => array('prompt' => 'เลือกประเภท หลักสูตร', 'style' => 'width:250px;')))); ?> 
            <?php echo $form->fileFieldGroup($model, 'image', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:190px;')))); ?>
            <?php echo $form->datePickerGroup($model, 'dayopencoure', array('widgetOptions' => array('options' => array(), 'htmlOptions' => array('style' => 'width:200px;')), 'prepend' => '<i class="glyphicon glyphicon-calendar"></i>')); ?>
            <?php echo $form->datePickerGroup($model, 'dayclose', array('widgetOptions' => array('options' => array(), 'htmlOptions' => array('style' => 'width:200px;')), 'prepend' => '<i class="glyphicon glyphicon-calendar"></i>')); ?>
            <?php echo $form->textFieldGroup($model, 'time', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:200px;')))); ?>
            <?php echo $form->textAreaGroup($model, 'location', array('widgetOptions' => array('htmlOptions' => array('rows' => 3, 'style' => 'width:300px;')))); ?>
<?php echo $form->radioButtonListGroup($model, 'typelocation', array('widgetOptions' => array('data' => array(' ภายใน', ' ภายนอก')))); ?>
<?php echo $form->textAreaGroup($model, 'discription', array('widgetOptions' => array('htmlOptions' => array('rows' => 3, 'style' => 'width:300px;')))); ?>
<?php echo $form->textFieldGroup($model, 'num_max', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:120px;')))); ?>
<?php echo $form->textFieldGroup($model, 'price', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:200px;')))); ?>
        <div class="form-group">
            <div class="col-sm-3">
            </div>
            <button type="button" style="margin-left: 5px;" data-toggle="modal" data-target="#costModal" class="btn btn-info">คำนวณราคาคอร์ส </button>
        </div>
        <div id="TextBoxesGroup_training_staf">
            <?php
            $mystafcousernum = 0;
            if (!$mytrue) {
                foreach ($mystafmodel as $key => $value) {
                    $mystafcousernum++;
                    array_push($stafarray, $value->emid);
                    ?>
                    <div id="TextBoxDiv_training_staf<?= $mystafcousernum ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ผู้ดูแลคอร์สคนที่ <?= $mystafcousernum ?></label>
                            <div class="col-sm-9">
                                <input style="width:250px;" class="form-control my_id_stafclass" placeholder="ผู้ดูแลคอร์ส" id="my_id_staf1" value="<?= $value->emid ?>" name="id_staf[]" type="text">
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>   
        <div style="text-align: center;padding-right: 40%;">
            <input type="hidden" id="userserver" value="<?= implode(',', $stafarray) ?>">
            <input type="hidden" id="mystafcousernum" value="<?= $mystafcousernum ?>">
            <button type="button" id="addButton_training_staf" class="btn btn-primary glyphicon glyphicon-plus" data-toggle="modal" data-target="#employeeModal">&nbsp;เพิ่มผู้ดูแลคอร์ส</button>    
            <button type="button" id="removeButton_training_staf" class="btn btn-danger glyphicon glyphicon-minus">&nbsp;ลบผู้ดูแลคอร์ส</button>
        </div> 
    </div>
    <div class="well">
        <legend>เพิ่มข้อมูลวันจัดอบรม</legend> 
        <div id="TextBoxesGroup_training">
<?php
$daytrainnum = 0;
if ($mytrue) {
    ?>
                <div id="TextBoxDiv_training1">
                    <table cellpadding="0" cellspacing="0" style="margin: 0 auto 0 10%;" border="0">  
                        <tr>
                            <td>วันที่ 1</td> 
                            <td>
                                <div class="form-group">
                                    <div class="input-group"><span class="input-group-addon">
                                            <i class="glyphicon glyphicon-calendar"></i></span>
                                        <input style="width:100px;" id="mydayfirst" class="form-control mydaymore ct-form-control" type="text"
                                               placeholder="2014-01-01" name="day[]">
                                    </div>
                                </div>
                            </td> 
                            <td>เวลา</td> 
                            <td>
                                <div class="form-group">
                                    <div class="input-group date form_time col-md-8"  id="datetimepicker_start" data-date="" data-date-format="hh:ii"  data-link-format="hh:ii">
                                        <input style="width:100px;" id="mydayfirsttime"  class="form-control mydayfirsttime" type="text" placeholder="12:00" name="timestart[]" value="">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                            </td>
                            <td>ถึง</td>
                            <td>
                                <div class="form-group">
                                    <div class="input-group date form_time col-md-8"  id="datetimepicker_end" data-date="" data-date-format="hh:ii"  data-link-format="hh:ii">
                                        <input style="width:100px;" id="mydaytwottime" class="form-control mydayendtime" type="text" placeholder="12:00" name="timeend[]" value="">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                            </td>
                        </tr> 
                        <tr>
                            <td colspan="6">
                                <textarea rows="3"  id="mydaydetail" class="form-control detailTraining" placeholder="รายละเอียดการสอน" name="detailTraining[]"></textarea>
                            </td> </tr>
                    </table>   
                </div>
    <?php
} else {
    foreach ($mydaymodel as $key => $value) {
        $daytrainnum++;
        array_push($training_array, $value->day);
        ?>
                    <div id="TextBoxDiv_training<?= $daytrainnum ?>">
                        <table cellpadding="0" cellspacing="0" style="margin: 0 auto 0 10%;" border="0">  
                            <tr>
                                <td>วันที่ <?= $daytrainnum ?></td> 
                                <td>
                                    <div class="form-group">
                                        <div class="input-group"><span class="input-group-addon">
                                                <i class="glyphicon glyphicon-calendar"></i></span>
                                            <input style="width:100px;" id="mydayfirst"  value="<?= $value->day ?>" class="form-control mydaymore ct-form-control" type="text"
                                                   placeholder="2014-01-01" name="day[]">
                                        </div>
                                    </div>
                                </td> 
                                <td>เวลา</td> 
                                <td>
                                    <div class="form-group">
                                        <div class="input-group date form_time col-md-8"  id="datetimepicker_start" data-date="" data-date-format="hh:ii"  data-link-format="hh:ii">
                                            <input style="width:100px;" id="mydayfirsttime"   class="form-control mydayfirsttime" type="text" placeholder="12:00" name="timestart[]" value="<?= $value->timestart ?>">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                        </div>
                                    </div>
                                </td>
                                <td>ถึง</td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group date form_time col-md-8"  id="datetimepicker_end" data-date="" data-date-format="hh:ii"  data-link-format="hh:ii">
                                            <input style="width:100px;" id="mydaytwottime" class="form-control mydayendtime" type="text" placeholder="12:00" name="timeend[]" value="<?= $value->timeend ?>">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                        </div>
                                    </div>
                                </td>
                            </tr> 
                            <tr>
                                <td colspan="6">
                                    <textarea rows="3"  id="mydaydetail" class="form-control detailTraining" placeholder="รายละเอียดการสอน" name="detailTraining[]"><?= $value->detail ?></textarea>
                                </td> </tr>
                        </table>   
                    </div>
        <?php
    }
}
?>
        </div>
        <div style="text-align: center;padding-right: 40%;">
            <input type="hidden" id="training_array" value="<?= implode(',', $training_array) ?>">
            <button type="button" id="addButton_training" class="btn btn-primary glyphicon glyphicon-plus">&nbsp;เพิ่มวันจัดอบรม</button>    
            <button type="button" id="removeButton_training" class="btn btn-danger glyphicon glyphicon-minus">&nbsp;ลบวันจัดอบรม</button>
        </div>

    </div>
    <div class="well"> 
        <div id='boxisupprier'>
                    <?php echo $form->textFieldGroup($model, 'supprier_id', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:200px;')))); //'disabled' => true  ?>
            <input type="hidden" id="isupprier_id" name="isupprier_id">
            <div class="form-group">
                <label class="col-sm-3 control-label">
                </label>
                <div class="col-sm-9">
        <?php
        $this->widget(
                'booster.widgets.TbButton', array(
            'label' => 'ค้นหาบริษัทรับจัดอบรม',
            'context' => 'primary',
            'htmlOptions' => array(
                'data-toggle' => 'modal',
                'data-target' => '#myModal',
            ),
                )
        );
        ?>
                </div></div> 
        </div> 
    </div>
    <div class="well form-actions" style="text-align: center;padding-right: 40%;">
<?php
$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'context' => 'primary',
    'label' => $model->isNewRecord ? 'บันทึก' : 'แก้ไข',
));
?>
<?php
$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'reset', 'label' => 'Reset')
);
?>
    </div> 
</fieldset>


<?php $this->endWidget(); ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">รายชื่อ บริษัทรับจัดอบรม </h4>
            </div>
            <div class="modal-body">
<?php echo $this->renderPartial('Supprier', array('model' => $supprier)); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal info  -->
<div class="modal fade" id="myModalinfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">รายชื่อบริษัทรับจัดอบรม </h4>
            </div>
            <div class="modal-body">
                <div id="datasup"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="employeeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">รายชื่อพนักงานแผนกทรัพยากรมนุษย์</h4>
            </div>
            <div class="modal-body">
<?php echo $this->renderPartial('employee', array('model' => $employee)); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal info empoyee  -->
<div class="modal fade" id="myModalinfoemployee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">ข้อมูลพนักงาน </h4>
            </div>
            <div class="modal-body">
                <div id="dataemployee"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal cost -->
<div class="modal fade" id="costModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">รายการค่าใช้จ่าย</h4>
            </div>
            <div class="modal-body">
<?php echo $this->renderPartial('costinfo', array('cost' => $cost)); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>





