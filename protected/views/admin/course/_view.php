<div class="view">
<table border="0" width="800" align="center">
		<tr>
           <td width="100">
		  <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/uploads/course/'.CHtml::encode($data->image),
      '',
      array('width'=>'100px','height'=>'100px','title'=>CHtml::encode($data->getAttributeLabel('image')))); ?>
		   </td>  		 
		    <td>
			<table border="0" >
			<tr>
			<td width="200" valign="top"><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></td>
            <td valign="top"><?php echo CHtml::encode($data->name);?></td>
			</tr>
			<tr>
			<td width="200" valign="top"><?php echo CHtml::encode($data->getAttributeLabel('categorycourse')); ?></td>
            <td valign="top"><?php echo CHtml::encode(Categorycourse::getlabelTypescourse($data->categorycourse));?></td>
			</tr>
			<tr>
			<td width="200" valign="top"><?php echo CHtml::encode($data->getAttributeLabel('price')); ?></td>
            <td valign="top"><?php echo CHtml::encode($data->price);?></td>
			</tr>
			</table>
		   </td>
		</tr>
</table>
    
    <?php /*
      <b><?php echo CHtml::encode($data->getAttributeLabel('dayclose')); ?>:</b>
      <?php echo CHtml::encode($data->dayclose); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('time')); ?>:</b>
      <?php echo CHtml::encode($data->time); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
      <?php echo CHtml::encode($data->location); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('typelocation')); ?>:</b>
      <?php echo CHtml::encode($data->typelocation); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('discription')); ?>:</b>
      <?php echo CHtml::encode($data->discription); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('num_max')); ?>:</b>
      <?php echo CHtml::encode($data->num_max); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
      <?php echo CHtml::encode($data->price); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('trainer')); ?>:</b>
      <?php echo CHtml::encode($data->trainer); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
      <?php echo CHtml::encode($data->active); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('categorycourse')); ?>:</b>
      <?php echo CHtml::encode($data->categorycourse); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('supprier_id')); ?>:</b>
      <?php echo CHtml::encode($data->supprier_id); ?>
      <br />

     */ ?>

</div>