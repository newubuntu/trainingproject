<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/front/sweetalert/sweet-alert.min.js');
//$cs->registerScriptFile($baseUrl.'/uploadify/myuploadify.js');
$cs->registerCssFile($baseUrl . '/front/sweetalert/sweet-alert.css');

$dayall = Daycoursetraining::getdayallcourse($model->course->cu_id);
$exdata = explode(',', $dayall);
$daynumss = count($exdata);
?>
<div class="well"> 
    <div  role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#tab_update_regis" id="home-tab" role="tab" data-toggle="tab" aria-controls="tab_update_regis" aria-expanded="true">แก้ไขข้อมูล</a></li>
            <li role="presentation" class="">
                <a href="#tab_course_detail" role="tab" id="profile-tab" data-toggle="tab" aria-controls="tab_course_detail" aria-expanded="false">ข้อมูลหลักสูตร</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_update_regis" aria-labelledby="tab_update_regis-tab">
                <!--################-->
                <div class="row">
                    <div class="col-xs-12">
                        <div style="border: 1px solid #DED;padding: 5px;">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div>
                                        <img  style="height: 150px;border: 1px solid #DED;padding:10px;" src="<?= Yii::app()->baseUrl; ?>/images/uploads/course/<?= $model->course->image ?>">
                                    </div>
                                </div>                
                                <div class="col-xs-10">
                                    <h4 style="padding:5px 15px 3px 20px; background:#428bca; border-radius:5px; margin-button:5px; margin-top:5px; color:#ffffff;">
                                        <span class="glyphicon glyphicon-play">&nbsp;</span>ข้อมูลการสมัคร</h4>
                                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                                        <div class="alert in fade alert-success">
                                            <?= Yii::app()->user->getFlash('success'); ?>
                                        </div> 
                                    <?php endif; ?>  
                                    <?php
                                    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                                        'id' => 'course-register-form',
                                        'enableAjaxValidation' => true,
                                        'type' => 'horizontal',
                                        'enableClientValidation' => true,
                                        'clientOptions' => array(
                                            'validateOnSubmit' => true,
                                          //  'validateOnChange' => false,
                                          //  'beforeValidate' => 'js:myAfterValidateFunction'
                                        ),
                                        'htmlOptions' => array('class' => 'well', 'enctype' => 'multipart/form-data'
                                        )
                                    ));
                                    ?>
                                    <?php echo $form->errorSummary($modelregis); ?>
                                    <?= $form->hiddenField($modelregis, 'employee_id', array('class' => 'form-control', 'value' => $model->employee->idemployee)); ?>        
                                    <?php echo $form->textFieldGroup($modelregis, 'idcourse', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:250px;', 'disabled' => true)))); ?>   
                                    <?php echo $form->textFieldGroup($modelregis, 'course_id', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:250px;')))); ?>   
                                    <?php
                                    echo $form->radioButtonListGroup($modelregis, 'approval', array('widgetOptions' => array('data' => array(
                                                '0' => 'รอดำเนินการ',
                                                '1' => 'อณุมัติแล้ว',
                                                '2' => 'ไม่อณุมัติ',
                                            )
                                        )
                                            )
                                    );
                                    ?>               
                                    <?php echo $form->textAreaGroup($modelregis, 'note', array('widgetOptions' => array('htmlOptions' => array('rows' => 3, 'style' => 'width:300px;')))); ?>
                                    <div class="well form-actions" style="text-align: center;">
                                        <?php
                                        $this->widget('booster.widgets.TbButton', array(
                                            'buttonType' => 'submit',
                                            'context' => 'primary',
                                            'label' => $modelregis->isNewRecord ? 'บันทึก' : 'แก้ไข',
                                        ));
                                        ?>
                                        <?php
                                        $this->widget('booster.widgets.TbButton', array(
                                            'buttonType' => 'reset', 'label' => 'Reset')
                                        );
                                        ?>
                                    </div> 
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>  
                <!---##############--->
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_course_detail" aria-labelledby="tab_course_detail-tab">
                <!--################-->
                <div class="row">
                    <div class="col-xs-12">
                        <div style="border: 1px solid #DED;padding: 5px;">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div>
                                        <img  style="height: 150px;border: 1px solid #DED;padding:10px;" src="<?= Yii::app()->baseUrl; ?>/images/uploads/course/<?= $model->course->image ?>">
                                    </div>
                                </div>                
                                <div class="col-xs-10">
                                    <h4 style="padding:5px 15px 3px 20px; background:#428bca; border-radius:5px; margin-button:5px; margin-top:5px; color:#ffffff;">
                                        <span class="glyphicon glyphicon-play">&nbsp;</span>รายละเอียดหลักสูตร</h4>
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <b>รหัสหลักสูตร :</b> 
                                                </td>
                                                <td>&nbsp;<?= $model->course->cu_id ?> </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>ชื่อหลักสูตร :</b>  
                                                </td>
                                                <td>&nbsp;<?=$model->course->name?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>ประเภท :</b> 
                                                </td>
                                                <td>&nbsp;<?= Categorycourse::getlabelTypescourse($model->course->categorycourse) ?></td>
                                            </tr> 
                                            <tr>
                                                <td>
                                                    <b> ราคา:</b> 
                                                </td>
                                                <td>&nbsp;<?= $model->course->price ?> </td>
                                            </tr>  
                                            <tr>
                                                <td>
                                                    <b>อบรมโดย :</b>
                                                </td>
                                                <td>&nbsp;<?php
                                                    if ($model->course->supprier_id != NUll) {
                                                        echo Supprier::getlabelsupprier($model->course->supprier_id);
                                                    } else {
                                                        echo "ผู้รับผิดชอบการบรม";
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>เปิดรับสมัครตั้งแต่:</b>
                                                </td>
                                                <td> &nbsp;<?= $model->course->dayopencoure ?> </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>ถึงวันที่ : </b>
                                                </td>
                                                <td>&nbsp;<?= $model->course->dayclose ?> </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>จำนวนวันอบรม :</b>
                                                </td>
                                                <td> &nbsp;<?= $daynumss ?>&nbsp; วัน</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>วันที่ทำการอบรม : </b> 
                                                </td>
                                                <td>&nbsp;<?= $dayall ?></td>
                                            </tr> 
                                            <tr>
                                                <td>
                                                    <b>รับสมัครจำนวน :</b>
                                                </td>
                                                <td><?= $model->course->num_max ?> &nbsp; คน</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>  
                <!---##############--->     
            </div>
        </div>
    </div>  
</div>
<!---
    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <b>รหัสพนักงาน :</b> 
                                                </td>
                                                <td>&nbsp;xx </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>ผลการอบรม:</b>  
                                                </td>
                                                <td>&nbsp;xx
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>ผลคะแนน:</b>  
                                                </td>
                                                <td>&nbsp;/</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b> รหัสหลักสูตร:</b> 
                                                </td>
                                                <td>&nbsp;dasd</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>  หัวข้อในการอบรม : </b>  	
                                                </td>
                                                <td>&nbsp;xxxx</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>ประเภท :</b> 
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>ผู้รับผิดชอบการบรม :</b>
                                                </td>
                                                <td>&nbsp;xx</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>เปิดรับสมัครตั้งแต่:</b>
                                                </td>
                                                <td> &nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>ถึงวันที่ : </b>
                                                </td>
                                                <td>&nbsp;s </td>
                                            </tr>	
                                            <tr>
                                                <td>
                                                    <b>วันที่ทำการอบรม : </b> 
                                                </td>
                                                <td>&nbsp;sd</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>จำนวนวัน :</b>
                                                </td>
                                                <td> &nbsp;s&nbsp; วัน</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>รับสมัครจำนวน :</b>
                                                </td>
                                                <td>s &nbsp; คน</td>
                                            </tr>

                                        </tbody>
                                    </table>
--->