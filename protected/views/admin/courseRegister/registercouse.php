<style type='text/css'>
    /* grid border */
    .grid-view table.items th, .grid-view table.items td {
        border: 1px solid gray !important;
    } 
    /* disable selected for merged cells */     
    .grid-view td.merge {
        background: none repeat scroll 0 0 #F8F8F8; 
    }
</style>
<div class="well"> 
    <h3>รายชื่อผู้สมัครทั้งหมด</h3>
    <?php
    $this->widget('ext.groupgridview.GroupGridView', array(
        'id' => 'register-course-grid',
        'dataProvider' => $dp,
        'itemsCssClass'=>'table table-bordered table-hover',
      //  'rowCssClassExpression'=>'($data->approval==4)?"info":($data->approval==1?"success":$data->approval==2?"danger":"")',
        'mergeColumns' => array('employee.iddept'),
        'columns' => array(
            array(
                'name' => 'employee.iddept',
                'header' => '<span style="color:#white;text-align:center;">แผนก</span>',
                'value' => 'Department::getlabeldepartmanets($data->employee->iddept)',
            ),
            'employee.firstname',
            'employee.lastname',
            array( 
                'name' => 'approval', 
                'value' => 'CourseRegister::getstatus($data->approval)',
            ),
       array('header' => '<span style="color:#000;">ดำเนินการ</span>',
                'class' => 'booster.widgets.TbButtonColumn',
                'template' => '{delete}{update}', //    'template'=>'{add} {list} {update} {print_act}',
                'buttons' => array(
                    'update' => array(
                        'label' => 'แก้ไข',
                        'icon' => 'fa fa-pencil-square-o',
                        'url' => 'Yii::app()->controller->createUrl("admin/courseRegister/UpdateUserDetail", array("cid"=>$data->course_id,"emid"=>$data->employee_id))',
                        'options' => array(
                            'class' => 'btn btn-small btn-warning', 'style' => 'margin:5px;',
                        ),
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->controller->createUrl("admin/courseRegister/Delete", array("rcid"=>$data->idcourse))',
                        'label' => 'ลบ',
                        // 'icon' => 'fa fa-times',
                        'options' => array(// this is the 'html' array but we specify the 'ajax' element
                            'confirm' =>'ยืนยันการลบข้อมูล !',
                            'class' => 'btn btn-small btn-danger',
                            'ajax' => array(
                                'type' => 'POST',
                                'url' => "js:$(this).attr('href')", // ajax post will use 'url' specified above
                                'success' => 'function(data){
                                if(data == 1){ 
                                 alert("ลบข้อมูลเรียบร้อยแล้วค่ะ !"); 
                                  $.fn.yiiGridView.update("register-course-grid");
                                   return false;
                                }else if(data == 2){
                                 alert("การลบข้อมูลผู้สมัครที่ลงทะเบียนไว้ไม่สำเร็จค่ะ");
                                 $.fn.yiiGridView.update("register-course-grid");
                                 return false;
                                }else{
                                 alert("การลบข้อมูล error !"); 
                                 $.fn.yiiGridView.update("register-course-grid");
                                 return false;
                                }
                            }',
                            ),
                        ),
                    ),
                /*
                  'delete' => array
                  (
                  'label' => 'ลบ',
                  'icon' => 'fa fa-times',
                  'url' => 'Yii::app()->controller->createUrl("admin/course/delete", array("id"=>$data->cu_id))',
                  'options' => array(
                  'class' => 'btn btn-small btn-danger',
                  ),
                  ),
                 */
                ),
                'htmlOptions' => array(
                    'style' => 'width: 200px;text-align:center;'
                ),
            ),
        ),
    ));
    ?>
</div>