 <?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('course-grid', {
data: $(this).serialize()
});
return false;
});
");
?>
<style type="text/css">
.text_center{
  text-align:center;
}
</style>
<div class="well">
 <h3>รายการประวัติหลักสูตร</h3>
<?php // echo CHtml::link('Advanced Search', '#', array('class' => 'search-button btn btn-primary')); ?>
<div class="search-form" style="display:none">
    <?php
    //$this->renderPartial('_search', array(
    //    'model' => $model,
   // ));
    ?>
</div><!-- search-form -->
<?php $form=$this->beginWidget('CActiveForm', array(
    'enableAjaxValidation'=>true,
)); ?>
<div style="margin-bottom:-46px;;" class="well">
<?php
echo CHtml::ajaxSubmitButton(
    $label = 'ลบข้อมูล', 
    array('admin/trainingHistory/Ajaxupdate'), array('success' => 'reloadGrid'),
    $htmlOptions=array ('confirm'=>'ยืนยันการลบข้อมูล','class' => 'btn btn-danger')
    );
?>  
</div>
 
<?php
$alert = 'ยืนยันการลบข้อมูล !';

$this->widget('booster.widgets.TbGridView', array(
    'id' => 'training-history-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped bordered',
    //'rowCssClassExpression'=>'($data->dayclose>$data->getdaynow())?"success":($data->dayclose<$data->getdaynow()?"danger":"")',
    'filter' => $model,
    'selectableRows'=>3,    
    'columns' => array(
        array(
            'id' => 'cu_id',
            'class' => 'CCheckBoxColumn',
           // 'selectableRows' => '50',
        ),
        'name',
        array(
          'name'=>'dayopencoure',
            'htmlOptions' => array(
             'style' => 'width: 150px;text-align:center;' 
            ),
        ),
        array(
          'name'=>'dayclose',
            'htmlOptions' => array(
             'style' => 'width: 150px;text-align:center;' 
            ),
        ), 
	array(
            'name'=>'categorycourse',
            //'header'=>'สถานะ',
            'htmlOptions'=> array( 'class' => 'form-control' ),
	    'filter'=> CHtml::dropDownList( 'TrainingHistory[categorycourse]', $model->categorycourse,
	     CHtml::listData(Categorycourse::getTypescourse(),'id', 'name'),
	    array( 'empty' => '-- หลักสูตร --','class' => 'form-control', )
	    ),
           'value'=>'Categorycourse::getlabelTypescourse($data->categorycourse)',
            'htmlOptions' => array(
             'style' => 'width: 180px;text-align:center;'//, 'class' => 'form-control'
            ),
	   ), 
        array('header' => '<span style="color:#428bca;">ดำเนินการ</span>',
           'class' => 'booster.widgets.TbButtonColumn',
            'template' => '{delete}{ansquest}', //    'template'=>'{add} {list} {update} {print_act}',
            'buttons' => array( 
            'delete'=> array(
                    'url' => 'Yii::app()->controller->createUrl("admin/trainingHistory/delete", array("id"=>$data->id,"cu_id"=>$data->cu_id))',
                    'label' => 'ลบ',
                   // 'icon' => 'fa fa-times',
                    'options' => array(// this is the 'html' array but we specify the 'ajax' element
                        'confirm' => $alert,
                        'class' => 'btn btn-small btn-danger',
                         'ajax' => array(
                            'type' => 'POST',
                            'url' => "js:$(this).attr('href')", // ajax post will use 'url' specified above
                            'success' => 'function(data){
                                if(data == 1){ 
                                 alert("ลบข้อมูลเรียบร้อยแล้วค่ะ !"); 
                                  $.fn.yiiGridView.update("course-grid");
                                   return false;
                                }else if(data == 2){
                                 alert("ไม่สามารถลบข้อมูลโปรดลองอีกครั้ง !");
                                 $.fn.yiiGridView.update("course-grid");
                                // window.location="index.php?r=admin/course/admin";
                                 return false;
                                }else{
                                 alert("การลบข้อมูล error !"); 
                                  $.fn.yiiGridView.update("course-grid");
                                 return false;
                                }
                            }',
                        ),
                    ),
                ),  
             'ansquest' => array(
                    'label' => 'ดูผลการประเมิน',
                    'icon' => 'fa fa-bar-chart-o',
                    'url' => 'Yii::app()->controller->createUrl("admin/questionresult", array("id"=>$data->cu_id))',
                    'options' => array(
                        'class' => 'btn btn-small btn-success', 'style' => 'margin:5px;',
                    ),
                ), 
            ),
            'htmlOptions' => array(
                'style' => 'width: 200px;', 'class' => 'text_center'
            ),
        )
    /*
      array(
      'class'=>'booster.widgets.TbButtonColumn',
      ), */
    ),'htmlOptions' => array(
                'style' => 'margin-top:0;'
            ),
   )
  );
?>
</div>
<script>
function reloadGrid(data) {
    $.fn.yiiGridView.update('training-history-grid');
}
</script>
<?php $this->endWidget(); ?>
 