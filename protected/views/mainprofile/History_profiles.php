<?php
if (count($modelhistory) > 0) {
    ?>
    <div id="my_box_trainning_history">
       <div class="panel panel-default" style="padding:2px;">
        <div class="panel-heading">ประวัติการเข้ารับการอบรม</div>
        <div class="panel-body">
             <?php
        $last_year = '';
        $is_first = TRUE;
        $output = '';
        $i = 0;
        foreach ($modelhistory as $key => $value) {
            $i++;
            $datayear = explode('-', $value['dmax']);
            $year = $datayear[0];
            if ($last_year != $year) { 
                if (!$is_first &&$i>2) {
                    print "</table>\n";
                }
                $is_first = FALSE;
                if($last_year!=''){
                print "<table  class=\"table table-bordered \">\n";
                print "<caption style='background: #428BCA;line-height: 40px;color: #fff;'><label>{$last_year}</label></caption>\n";
                print "<th width='200'>รหัสสูตร</th> <th>ชื่อหลักสูตร</th>\n";
                print $output; 
                }
                $output = '';
                $last_year = $year;
            }
            $output .= render($value);
        }
        print "</table>\n";
        print "<table class=\"table table-bordered\">\n";
        print "<caption style='background: #428BCA;line-height: 40px;color: #fff;'><label>{$last_year}</label></caption>\n";
        print "<th width='200'>รหัสสูตร</th> <th>ชื่อหลักสูตร</th>\n";
        print $output;
        print "</table>\n";
        ?> 
        </div>
        </div>
      	
    </div> 	
<?php
}

function render($record) {
    $link=Yii::app()->createUrl('historycourse/detail',array('id'=>$record['cu_id']));
    $output = "<tr>\n";
    $output .= "<td width='200'><a href='{$link}'>{$record['cu_id']}</a></td>\n";
    $output .= "<td >{$record['name']}</td>\n"; 
    $output .= "</tr>\n";
    return $output;
}
?>